package com.mycompany.httpserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
/** 
 * HTTP Response = Status-Line 
 *   *(( general-header | response-header | entity-header ) CRLF) 
 *   CRLF 
 *   [message-body] 
 *   Status-Line=Http-Version SP Status-Code SP Reason-Phrase CRLF 
 * 
 */
public class Response {
	private static final int BUFFER_SIZE=1024;
	Request request;
	OutputStream output;
	public Response(OutputStream output){
		this.output=output;
	}
	public void setRequest(Request request){
		this.request=request;
	}
	public void sendStaticResource()throws IOException{
		FileInputStream fis=null;
		try {
			File file=new File(HttpServer.WEB_ROOT,request.getUri());
			Long file_size = file.length();
			byte[] bytes=new byte[file_size.intValue()];
			if(file.exists()){
				fis=new FileInputStream(file);
				fis.read(bytes);
				String body = new String(bytes);
				String errorMessage="HTTP/1.1 200 OK\r\n"+ 
				        "Content-Type:application/json\r\n"+ 
				        "Content-Length:" + body.length() + "\r\n"+ 
				        "\r\n"+ 
				        body;
				output.write(errorMessage.getBytes());
			} else{
				//file not found 
				String errorMessage="HTTP/1.1 404 Not Found\r\n"+ 
				        "Content-Type:text/html\r\n"+ 
				        "Content-Length:23\r\n"+ 
				        "\r\n"+ 
				        "<h1>File Not Found</h1>";
				output.write(errorMessage.getBytes());
			}
		}
		catch (Exception e) {
			//System.out.println(e.toString());
			String errorMessage="HTTP/1.1 404 File Not Found\r\n"+ 
				        "Content-Type:text/html\r\n"+ 
				        "Content-Length:23\r\n"+ 
				        "\r\n"+ 
				        "<h1>File Not Found</h1>";
			output.write(errorMessage.getBytes());
		}
		finally{
			if(fis!=null){
				fis.close();
			}
		}
	}
}