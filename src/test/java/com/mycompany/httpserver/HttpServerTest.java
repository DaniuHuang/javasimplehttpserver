package com.mycompany.httpserver;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
public class HttpServerTest
{

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @Test
    public void testHttpServerConstructor() {
        try {
            new HttpServer();
        } catch (Exception e) {
            fail("Construction failed.");
        }
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

}
