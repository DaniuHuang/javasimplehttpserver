#based on this image
FROM fiadliel/java8-jre:latest

#run mkdir command in the docker in the building stage.
RUN mkdir -p code/

#Copy the files you have created earlier into our image by using COPY command.
#If <dest> doesn’t exist, it is created along with all missing directories in its path.
COPY target/javahttpserver-1.0-SNAPSHOT.jar code/target/
COPY src/main/java/webroot code/target/webroot/

#set working dir based on earlier WORKDIR, NOW become ${WORKDIR}/code/
WORKDIR code/target/

#ENV JAVA_HOME=/usr/lib/jvm/java-8-oracle
#ENV PATH=$PATH:$JAVA_HOME/bin
#ENV CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar

#running the application. CMD will not run in building stage, whereas run when you execute docker run command.
CMD ["java", "-jar", "javahttpserver-1.0-SNAPSHOT.jar"]